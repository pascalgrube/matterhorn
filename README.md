# Notice

This is a fork of the [Official Opencast Matterhorn
Repository](https://bitbucket.org/opencast-community/matterhorn).


This fork is used to develop  additional features and fixes developed by Pascal Grube  which are not yet merged into the
official Opencast Matterhorn repository.
=======
Opencast
========

Open Source Lecture Capture & Video Management for Education

Opencast is a free, open-source platform to support the management of
educational audio and video content. Institutions can use Opencast to
produce lecture recordings, manage existing video, serve designated
distribution channels, and provide user interfaces to engage students with
educational videos.


Installation
------------

Installation instructions can be found locally at

    docs/guides/admin/docs

or on our documentation server at:

 * [Opencast Documentation](http://docs.opencast.org)


Community
---------

More information about the community can be found at:

* [Opencast Website](http://opencast.org/)
* [Mailing lists, IRC, …](http://opencast.org/community)
* [Issue Tracker](http://opencast.jira.com/)
